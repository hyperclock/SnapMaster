# SnapMaster

is a modifeied version of Remastersys. The mods I make or have made are biased 
towards the HubShark OS project.

**SnapMaster** is (re)designed to build the CD/DVD images for the HubShark OS project,
using Ubuntu as it's base system.


## License

Remastersys: _GPLv2_

SnapMaster: _GPLv3_
